# CompanionBot #

A bot for the Telegram Messenger.

![Companion Cube](http://images.wikia.com/half-life/en/images/3/30/Companion_Cube_p2.png)

### Current Commands ###
```
/help       - Show some basic information about the bot.
/version    - Displays a detailed list of command.
/commands   - Return a list of all commands.
/rand       - Returns a random number based on the users input.
/dice       - Recreates a dice trow.
/8ball      - Ask the magic 8 ball a question.
```