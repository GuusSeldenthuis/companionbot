'''
Created by Guus Seldenthuis & Rick van der Plas
http://guusseldenthuis.nl/
http://rickvdplas.nl/

ToDo:
   -- weather
   -- search imgur
   --
   --
'''
# Bot settings & static values
botVersion = 'v0.51.3'
botLastEdit = 'June 28th 2015'

import json
import logging
import urllib
import urllib2

# Own modules
import random
import hashlib

# googles stuff
from google.appengine.api import urlfetch
from google.appengine.ext import ndb
import webapp2

TOKEN = '116060072:AAE53h4jcsftb6Sq0f5o-Ec3krkJDThedQk'

BASE_URL = 'https://api.telegram.org/bot' + TOKEN + '/'

# ================================

class EnableStatus(ndb.Model):
    # key name: str(chat_id)
    enabled = ndb.BooleanProperty(indexed=False, default=False)


# ================================

def setEnabled(chat_id, yes):
    es = EnableStatus.get_or_insert(str(chat_id))
    es.enabled = yes
    es.put()

def getEnabled(chat_id):
    es = EnableStatus.get_by_id(str(chat_id))
    if es:
        return es.enabled
    return False

# ================================

class MeHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getMe'))))


class GetUpdatesHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getUpdates'))))


class SetWebhookHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        url = self.request.get('url')
        if url:
            self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'setWebhook', urllib.urlencode({'url': url})))))


class WebhookHandler(webapp2.RequestHandler):
    def post(self):
        urlfetch.set_default_fetch_deadline(60)
        body = json.loads(self.request.body)
        self.response.write(json.dumps(body))

        #logging.info('request received.')
        #logging.info(body)

        update_id = body['update_id']
        message = body['message']
        message_id = message.get('message_id')
        date = message.get('date')
        text = message.get('text')
        fr = message.get('from')
        chat = message['chat']
        chat_id = chat['id']

        logging.info(fr)
        logging.info(text)

        if not text:
            logging.info('no text')
            return

        # Custom methods
        def IsInt(s):
            try: 
                int(s)
                return True
            except ValueError:
                return False

        def reply(msg, reply=False):
            if reply == True:
                resp = urllib2.urlopen(BASE_URL + 'sendMessage', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'text': msg,
                    'disable_web_page_preview': 'true',
                    'reply_to_message_id': str(message_id),
			     })).read()
            else:
                resp = urllib2.urlopen(BASE_URL + 'sendMessage', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'text': msg,
                    'disable_web_page_preview': 'true',
                })).read()
            logging.info('CompanionBot: '+msg)
            #logging.info(resp)

        messageInput = "make this a string"
        messageInput = text.lower()

        # Commands
        if messageInput.startswith('/'):
            if '/startrobot' in messageInput:
                reply('Bot enabled')
                setEnabled(chat_id, True)

            elif '/stoprobot' in messageInput:
                reply('Bot disabled (this is a lie)')
                setEnabled(chat_id, False)

            elif '/help' in messageInput:
                reply('CompanionBot '+ botVersion +'\nCreated by \n @Panacea & @rickvanderplas\n\n/commands for a detailed list of all commands.')

            elif '/hoi' in messageInput:
                reply('Hello?')

            elif '/commands' in messageInput:
                reply('/help : Displays some info\n/commands : this message\n/version : Displays version info.\n/rand [min] [max] : Outputs a random number.\n/8ball : Answers all of your question.\n/diceroll : Throw a random number between 1 and 6.\n/say Let the bot say something.\n/hash [md5,sha1,sha256] [input] : Outputs a hash.')

            elif '/version' in messageInput:
                reply('CompanionBot '+ botVersion +'\n'+ botLastEdit)

            elif '/rand' in messageInput:
                if messageInput == '/rand' or messageInput == '/rand help':
                    reply('rand use: /rand [min number] [max number]', True)
                    return

                try:
                    randInput = messageInput.split(' ')
                    if IsInt(randInput[1]) == True and IsInt(randInput[2]) == True:
                        if int(randInput[1]) < int(randInput[2]):
                            randomInt = random.randrange(int(randInput[1]),int(randInput[2]))
                            reply(str(randomInt), True)
                        else:
                            reply('Input error, 2nd number must be greater than its first one.', True)
                    else:
                        reply('One or both inputs are invalid.', True)
                except IndexError:
                    reply('Input error.', True)

            elif '/diceroll' in messageInput:
                randomInt = random.randrange(1,6)
                reply('The dice rolled '+ str(randomInt)+'.', True)

            elif '/hash help' in messageInput:
                reply("/hash [md5,sha1,sha256] [input]", True)

            elif '/hash md5' in messageInput:
                reply(hashlib.md5(text[10:]).hexdigest(), True)

            elif '/hash sha1' in messageInput:
                reply(hashlib.sha1(text[11:]).hexdigest(), True)

            elif '/hash sha256' in messageInput:
                reply(hashlib.sha256(text[13:]).hexdigest(), True)

            elif '/say' in messageInput:
                if text[5:] != '':
                    reply(text[5:], True)
                else:
                    reply('say use: /say [message]')

            elif '/8ball' in messageInput:

                if messageInput == '/8ball' or messageInput[-1:] != '?':
                    reply('I need a question.', True)
                    return

                eightballresp = []
                eightballresp.append('It is certain\'')
                eightballresp.append('It is decidedly so')
                eightballresp.append('Without a doubt')
                eightballresp.append('Yes definitely')
                eightballresp.append('You may rely on it')
                eightballresp.append('As I see it, yes')
                eightballresp.append('Most likely')
                eightballresp.append('Outlook good')
                eightballresp.append('Yes')
                eightballresp.append('Signs point to yes')
                eightballresp.append('Reply hazy try again')
                eightballresp.append('Ask again later')
                eightballresp.append('Better not tell you now')
                eightballresp.append('Cannot predict now')
                eightballresp.append('Concentrate and ask again')
                eightballresp.append('Don\'t count on it')
                eightballresp.append('My reply is no')
                eightballresp.append('My sources say no')
                eightballresp.append('Outlook not so good')
                eightballresp.append('Very doubtful')

                reply(eightballresp[random.randrange(0,19)], True)

            '''elif '/google' in messageInput:
                query = text.encode('utf-8')
                query = query[7:]
                query = urllib.urlencode ( { 'q' : query } )
                url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query
                logging.info(url)
                search_response = urllib.urlopen(url)
                search_results = search_response.read()
                results = json.loads(search_results)
                data = results[ 'responseData' ]
                hits = data['results']
                for h in hits: reply(h['url'])'''

            # else:
                # geen command gevonden
        elif 'cake' in messageInput:
            reply('The cake is lie!')

app = webapp2.WSGIApplication([
    ('/me', MeHandler),
    ('/updates', GetUpdatesHandler),
    ('/set_webhook', SetWebhookHandler),
    ('/webhook', WebhookHandler),
], debug=True)
